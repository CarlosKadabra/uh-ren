import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { RouterHomeComponent } from './router-components/router-home/router-home.component';
import { AdminComponent } from './components/admin/admin.component';
import { CreateAndUpdateComponent } from './components/create-and-update/create-and-update.component';

import { LoginGuard } from '../app/guards/login.guard'
import { ErrorComponent } from './components/error/error.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'home/:id', component: RouterHomeComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'home/admin/:id/createorupdate', component: CreateAndUpdateComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'home/admin/:id', component: AdminComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'about', component: AboutComponent,
    canActivate: [LoginGuard]
  },
  { path: 'error', component: ErrorComponent },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
