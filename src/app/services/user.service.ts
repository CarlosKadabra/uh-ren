import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginForm } from '../models/login-form.model';
import { User } from '../models/user-form.model';

import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  userLogin$;
  userLoggedId$: Observable<any>;
  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) {
  }

  login(formData: LoginForm): Observable<any> {
    // this.userLogin$ = this.http.post(`${this.baseUrl}user/login`, formData);
    return this.userLogin$ = this.http.post(`${this.baseUrl}user/login`, formData);
  }

  getUserById(id) {
    return this.http.get(`${this.baseUrl}users/user/${id}`);
  }
  logout() {
    localStorage.clear;
    //this.isLoggedin = false;
  }

  getAllUsers() {
    return this.http.get<User[]>(`${this.baseUrl}users`);
  }

  setUserHours(id, user) {
    return this.http.put(`${this.baseUrl}users/${id}`, user)
  }

  createUser(user) {
    return this.http.post(`${this.baseUrl}users`, user);
  }

}
