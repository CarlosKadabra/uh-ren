import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from "rxjs/operators";
import { UsersList } from './admin';
import { User } from 'src/app/models/user-form.model';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  baseUrl = environment.baseUrl;
  usersList$: Observable<UsersList>;

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<UsersList> {
    return this.usersList$ = this.http.get(`${this.baseUrl}users`).pipe(
      map((users: UsersList) => users));
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}users`);
  }

  getUser(id: String): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}users/user/${id}`);
  }

  postUser(user: User) {
    return this.http.post(`${this.baseUrl}users`, user);
  }

  //EDIT user
  updateUser(user) {
    return this.http.put(`${this.baseUrl}users` + `/${user.uid}`, user);

  }

  //DELETE user
  deleteUser(id: any): Observable<any> {
    return this.http.delete(`${this.baseUrl}users/${id}`);
  }

}
