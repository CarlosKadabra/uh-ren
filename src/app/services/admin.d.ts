import { StringMap } from "@angular/compiler/src/compiler_facade_interface";

export type UsersList = User[];
export interface User {


    name: string,
    lastName: string,
    email: string,
    password: string,
    role: string,
    profilePic: string,
    totalHoursEmployer: number
    date: [
        {
            day: Date,
            start: string,
            end: string,
            totalhours: Number
        },
    ]
}