import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable, Subject } from 'rxjs';
import { catchError, delay, switchMap } from 'rxjs/operators';
import { AdminService } from 'src/app/services/admin.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-router-home',
  templateUrl: './router-home.component.html',
  styleUrls: ['./router-home.component.scss']
})
export class RouterHomeComponent implements OnInit {
  admin$ = false;
  usersList$: Observable<any>;

  userInfo$;
  userInfoById: any;
  private errorSubject = new Subject<boolean>();
  error$: Observable<boolean>;
  constructor(private adminService: AdminService, private activatedRouter: ActivatedRoute, private userService: UserService) { }

  ngOnInit(): void {
    this.usersList$ = this.adminService.getAllUsers();
    this.error$ = this.errorSubject.asObservable();
    this.userInfo$ = this.activatedRouter.params
      .pipe(
        delay(0),
        switchMap(({ id }) => {
          this.errorSubject.next(false);
          return this.userService.getUserById(id)
            .pipe(
              catchError((err) => {
                this.errorSubject.next(true);
                return EMPTY;
              })
            )
        }));
        console.log(this.userInfo$);
    this.userInfo$.subscribe((res) => {
      this.userInfoById = res;
      console.log(this.userInfoById, 'userInfoById');
    });
  }

}
