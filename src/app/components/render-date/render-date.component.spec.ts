import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenderDateComponent } from './render-date.component';

describe('RenderDateComponent', () => {
  let component: RenderDateComponent;
  let fixture: ComponentFixture<RenderDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RenderDateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenderDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
