import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Scheduler } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { AdminService } from 'src/app/services/admin.service';
import { UserService } from 'src/app/services/user.service';
import { Schedule } from './d';

@Component({
  selector: 'app-render-date',
  templateUrl: './render-date.component.html',
  styleUrls: ['./render-date.component.scss']
})
export class RenderDateComponent implements OnInit {

  @Input() userInfo;
  submit = false;

  user: any;

  model: NgbDateStruct;
  schedule: FormGroup;

  scheduleForUser: Schedule;
  constructor(private userService: UserService, private adminService: AdminService) {
    // this.scheduleForUser.date = '';
    // this.scheduleForUser.start = '';
    // this.scheduleForUser.end = '';
  }

  ngOnInit(): void {
    this.schedule = new FormGroup({
      date: new FormControl('', Validators.required),
      in: new FormControl('', Validators.required),
      out: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    this.submit = true;
    const newTime = {
      day: `${this.schedule.value.date.day}/${this.schedule.value.date.month}/${this.schedule.value.date.year}`,
      start: `${this.schedule.value.in.hour}:${this.schedule.value.in.minute}`,
      end: `${this.schedule.value.out.hour}:${this.schedule.value.out.minute}`
    }
    this.userService.getUserById(this.userInfo.uid).subscribe(res => {
      this.user = res;
      this.user.date.push(newTime);
      const newUser = {
        position: `${this.user.position}`,
        role: `${this.user.role}`,
        totalHoursEmployer: `${this.user.totalHoursEmployer}`,
        name: `${this.user.name}`,
        lastName: `${this.user.lastName}`,
        password: `${this.user.password}`,
        email: `${this.user.email}`,
        date: this.user.date,
        uid: this.user.uid
      }
      this.adminService.updateUser(newUser);
      console.log('sus', newUser);
    })
  }
}
