export type Schedule = {
    date: string,
    start: string,
    end: string

}