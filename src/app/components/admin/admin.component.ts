import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from 'src/app/models/user-form.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, Observable, Subject } from 'rxjs';
import { catchError, delay, switchMap } from 'rxjs/operators';
import { AdminService } from 'src/app/services/admin.service';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  usersList$: Observable<any>;

  public totalUsers;
  users: User [];
  id : string;

  userInfo$;
  userInfoById: any;
  private errorSubject = new Subject<boolean>();
  error$: Observable<boolean>;
  constructor(
    private adminService: AdminService, 
    private activatedRouter: ActivatedRoute, 
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.usersList$ = this.adminService.getAllUsers();
    this.error$ = this.errorSubject.asObservable();
    this.userInfo$ = this.activatedRouter.params
      .pipe(
        delay(0),
        switchMap(({ id }) => {
          this.errorSubject.next(false);
          return this.userService.getUserById(id)
            .pipe(
              catchError((err) => {
                this.errorSubject.next(true);
                return EMPTY;
              })
            )
        }));
      this.userInfo$.subscribe((res) => {
      this.userInfoById = res;
    });
    //ALL OBJECT USERS
    this.adminService.getUsers().subscribe(data =>{
      this.users=data["users"];
      console.log(this.users[1]);
    });

    //EDIT users

    //this.adminService.updateUser()

    //DELETE user
      //DELETE user

      
  }

  delete(id: string) {
    console.log(this.id);
    if(window.confirm('¿Quiere borrar este usuario?')) {
      this.adminService.deleteUser(id).subscribe((res) => {
      })
    }0
    this.usersList$ = this.adminService.getAllUsers();
  }

  updateUser(idUser: string){
    //this.adminService.updateUser(user);
    this.router.navigate(['/home/admin/'+idUser+"/createorupdate", { id: idUser }]);
  }
}