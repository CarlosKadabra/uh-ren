import { Component, Input ,OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Input() userInfo;

  constructor( private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  logoutUser(): void {
    this.userService.logout();
    this.router.navigateByUrl('');
  }

}