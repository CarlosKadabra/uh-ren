import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenderAsideComponent } from './render-aside.component';

describe('RenderAsideComponent', () => {
  let component: RenderAsideComponent;
  let fixture: ComponentFixture<RenderAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenderAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenderAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
