import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-render-aside',
  templateUrl: './render-aside.component.html',
  styleUrls: ['./render-aside.component.scss']
})
export class RenderAsideComponent implements OnInit {
  @Input() userInfo;


  constructor() { }

  ngOnInit(): void {
  }

}
