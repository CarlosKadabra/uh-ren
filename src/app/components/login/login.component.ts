import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginForm } from 'src/app/models/login-form.model';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: LoginForm = new LoginForm();
  remember = false;
  corporativeMail: string = 'admin@uhren.com'

  constructor(private router: Router, private fb: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
  }

  login(form: NgForm) {
    if (form.invalid) { return; }
    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Please wait...'
    });
    Swal.showLoading();
    this.userService.login(this.user)
      .subscribe(resp => {
        Swal.close();
        if (resp.role == 'ADMIN_ROLE') {
          this.router.navigate(['/home/admin', `${resp.userID}`]);
        } else {
          this.router.navigate(['/home', `${resp.userID}`]);
        }

      }, (err) => {
        Swal.close();
        Swal.fire({
          icon: 'error',
          title: 'Error to authenticate'
        });
      });
  }
}
