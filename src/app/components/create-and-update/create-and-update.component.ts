import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user-form.model';
import { AdminService } from 'src/app/services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from "rxjs/operators";

@Component({
  selector: 'app-create-and-update',
  templateUrl: './create-and-update.component.html',
  styleUrls: ['./create-and-update.component.scss']
})

@Injectable({
  providedIn: 'root'
})

export class CreateAndUpdateComponent implements OnInit {

  private user: User;
  userInfo: User;
  private id: String;
  user$: Observable<User>;
  uid: String;

  constructor(private adminService: AdminService, private router: Router, private activatedRouter: ActivatedRoute) { }

  ngOnInit(): void {
    //this.adminService.getAllUsers();
    this.user$ = this.activatedRouter.paramMap.pipe(
      switchMap(params => {
        this.id = String(params.get('id'));
        return this.adminService.getUser(this.id);
      })
    );
    this.user$.subscribe((res) => {
      this.userInfo = res;
      console.log(this.userInfo);
    });
  }

  createorupdate() {
    console.log("ID: " + this.userInfo.name);
    
    
    if (this.userInfo.uid == undefined) {
      this.adminService.postUser(this.userInfo).subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/home/admin']);
        },
        error => {
          console.log(error);
        }
      )
    } else {
      this.adminService.updateUser(this.userInfo).subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/home/admin/'+'/'+'6099ae0b20ed235037e624e0']);
        },
        error => {
          console.log(error);
        }
      )
    }
  }

}
