export class User{

        name: string;
        lastName: string;
        password: string;
        email: string;
        role: string;
        totalHoursEmployer: Number;
        position: string;
        date: {
            day:String,
            start: string,
            end: string,
            totalhours: number,
        };
        uid: string
        
}