import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterHomeComponent } from './router-components/router-home/router-home.component';
import { RenderDateComponent } from './components/render-date/render-date.component';
import { RenderAsideComponent } from './components/render-aside/render-aside.component';
import { NgbModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './components/about/about.component';
import { ErrorComponent } from './components/error/error.component';
import { AdminComponent } from './components/admin/admin.component';
import { CreateAndUpdateComponent } from './components/create-and-update/create-and-update.component';


@NgModule({
  declarations: [
    AppComponent,
    RouterHomeComponent,
    RenderDateComponent,
    RenderAsideComponent,
    NavBarComponent,
    LoginComponent,
    AboutComponent,
    ErrorComponent,
    AdminComponent,
    CreateAndUpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    CommonModule,
    NgbTimepickerModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
